<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../ImageFormats.cpp" line="187"/>
        <source>current image format not supported</source>
        <translation>当前摄像头捕获的图像格式不支持</translation>
    </message>
</context>
<context>
    <name>StillImageWorker</name>
    <message>
        <location filename="../WebcamWindow.cpp" line="560"/>
        <source>unknown</source>
        <translation type="unfinished">无名</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="580"/>
        <source> has already beed saved.</source>
        <translation type="unfinished"> 已保存.</translation>
    </message>
</context>
<context>
    <name>WebcamWindow</name>
    <message>
        <location filename="../WebcamWindow.cpp" line="47"/>
        <source>Turn On</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="48"/>
        <source>Turn Off</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="49"/>
        <source>Capture</source>
        <translation>拍一张</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="50"/>
        <source>Capture Three</source>
        <translation>拍三张</translation>
    </message>
    <message>
        <source>Record Video</source>
        <translation type="vanished">录屏</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="51"/>
        <source>Start Record Video</source>
        <translation>开始录制视频</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="52"/>
        <source>Stop Record Video</source>
        <translation>结束录制视频</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="53"/>
        <source>Devices</source>
        <translation>设备列表</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="55"/>
        <source>Resolutions</source>
        <translation>分辨率</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="57"/>
        <source>Output Path</source>
        <translation>输出路径</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="59"/>
        <source>Browser</source>
        <translation>浏览</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="60"/>
        <source>name</source>
        <translation>患者名称</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="74"/>
        <source>Webcam</source>
        <translation>德辉医疗口腔监测</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="240"/>
        <location filename="../WebcamWindow.cpp" line="444"/>
        <source>unknown</source>
        <translation>无名</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="260"/>
        <source> has already beed saved.</source>
        <translation> 已保存.</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="478"/>
        <source> start recording.</source>
        <translation> 开始录制.</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="485"/>
        <source>Video finish record.</source>
        <translation>视频录制完毕.</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="490"/>
        <source>Get output directory</source>
        <translation>获取输出路径</translation>
    </message>
</context>
</TS>
